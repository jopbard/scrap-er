from flask import Blueprint,render_template,redirect,url_for,flash,abort,request,jsonify
from flask_login import current_user
from forex_python.converter import CurrencyRates
from app import (
    app,
    db,
    Business,
    Broker,
    Office,
    get_industries_obj,
    get_biz_types_obj,
)
import os
import json

stat_blueprint = Blueprint('stat',__name__)
@stat_blueprint.route('/jsooon')
def jsooon():
    pass
@stat_blueprint.route('/jsoon')
def jsoon():
    offices = Office.query.all()
    for office in offices:
        brokers_count = Broker.query.filter_by(office_id=office.id).count()
        if brokers_count <= 0:
            continue
        else:
            brokers = Broker.query.filter_by(office_id=office.id).all()
            for broker in brokers:
                for biz in broker.businesses:
                    r_exists = office.businesses.filter(Business.id==biz.id).scalar() is not None 
                    if r_exists:
                        continue
                    else:
                        office.businesses.append(biz)
    db.session.commit()
    return 'done'            

@stat_blueprint.route('/test')
def test():
    # biz_list = []

    # ph_biz_list = []
    # ph_broker_count = []
    # offices = Office.query.filter_by(country_id=1).all()
    # for office in offices:
    #     d = {}
    #     d['office'] = office.title
    #     d['count'] = office.businesses.count()
    #     brokers_count = Broker.query.filter_by(office_id=office.id).count()
    #     if brokers_count > 0:
    #         ph_broker_count.append(brokers_count) 
    #     ph_biz_list.append(d)
    # biz_list.append(ph_biz_list)

    # us_biz_list = []
    # offices = Office.query.filter_by(country_id=2).all()
    # us_broker_count = []
    # for office in offices:
    #     d = {}
    #     d['office'] = office.title
    #     d['count'] = office.businesses.count() 
    #     brokers_count = Broker.query.filter_by(office_id=office.id).count()
    #     if brokers_count > 0:
    #         us_broker_count.append(brokers_count)
    #     us_biz_list.append(d)
    # biz_list.append(us_biz_list)

    # au_biz_list = []
    # au_broker_count = []
    # offices = Office.query.filter_by(country_id=3).all()
    # for office in offices:
    #     d = {}
    #     d['office'] = office.title
    #     d['count'] = office.businesses.count()
    #     brokers_count = Broker.query.filter_by(office_id=office.id).count()
    #     if brokers_count > 0:
    #         au_broker_count.append(brokers_count) 
    #     au_biz_list.append(d)
    # biz_list.append(au_biz_list)

    # nz_biz_list = []
    # nz_broker_count = []
    # offices = Office.query.filter_by(country_id=4).all()
    # for office in offices:
    #     d = {}
    #     d['office'] = office.title
    #     d['count'] = office.businesses.count()
    #     brokers_count = Broker.query.filter_by(office_id=office.id).count()
    #     if brokers_count > 0:
    #         nz_broker_count.append(brokers_count) 
    #     nz_biz_list.append(d)
    # biz_list.append(nz_biz_list)

    # c = CurrencyRates()
    # currency_rates = []
    # ph_biz = Business.query.filter_by(country_id=1).count()
    # ph_4s = Business.query.filter_by(country_id=1,sold=False).count()
    # ph_percentage = percentage(ph_4s, ph_biz)

    # us_biz = Business.query.filter_by(country_id=2).count()
    # us_4s = Business.query.filter_by(country_id=2,sold=False).count()
    # us_percentage = percentage(us_4s, us_biz)

    # au_biz = Business.query.filter_by(country_id=3).count()
    # au_4s = Business.query.filter_by(country_id=3,sold=False).count()
    # au_percentage = percentage(au_4s, au_biz)

    # nz_biz = Business.query.filter_by(country_id=4).count()
    # nz_4s = Business.query.filter_by(country_id=4,sold=False).count()
    # nz_percentage = percentage(nz_4s, nz_biz)
    

    # ph_brokers = sum(ph_broker_count)
    # us_brokers = sum(us_broker_count)
    # au_brokers = sum(au_broker_count)
    # nz_brokers = sum(nz_broker_count)

    # ph_list = [ph_4s, ph_biz, ph_percentage, ph_brokers]
    # us_list = [us_4s, us_biz, us_percentage, us_brokers]
    # au_list = [au_4s, au_biz, au_percentage, au_brokers]
    # nz_list = [nz_4s, nz_biz, nz_percentage, nz_brokers]

    #bankers rounding - https://en.wikipedia.org/wiki/Rounding#Round_half_to_even
    # currency_rates.append({'rate':str(round(c.get_rate('USD', 'PHP'), 2)),'currencies':'aud/php'})
    # currency_rates.append({'rate':str(round(c.get_rate('AUD', 'PHP'), 2)),'currencies':'usd/php'})
    # currency_rates.append({'rate':str(round(c.get_rate('NZD', 'PHP'), 2)),'currencies':'nzd/php'})

    #return render_template('stat.html',ra=request.user_agent, currency_rates=currency_rates, biz_list=biz_list, ph_list=ph_list, us_list=us_list, au_list=au_list, nz_list=nz_list)
    #return jsonify( currency_rates=currency_rates, biz_list=biz_list, ph_list=ph_list, us_list=us_list, au_list=au_list, nz_list=nz_list)
    filename = os.path.join(app.static_folder, 'stat-data.json')
    with open(filename) as blog_file:
        data = json.load(blog_file)
    return render_template('stat.html',ra=request.user_agent, currency_rates=data['currency_rates'], biz_list=data['biz_list'], ph_list=data['ph_list'], us_list=data['us_list'], au_list=data['au_list'], nz_list=data['nz_list'])
def percentage(part, whole):
  return 100 * float(part)/float(whole)