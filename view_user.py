from flask import Blueprint,render_template,redirect,url_for,flash,abort,request
from app import app,db,mail,User,UserGeoIP,simple_geoip
from tokenizer import generate_confirmation_token,confirm_token
from form_user import LoginForm,RegistrationForm
from flask_login import login_user,login_required,logout_user
from flask_mail import Message
import datetime

users_blueprint = Blueprint('users',__name__)

def send_email_confirmation(to, html):
    msg = Message("Confirm Your Email",sender="glinkscraper@gmail.com",
                        recipients=[to], html=html)
    mail.send(msg)

@users_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You logged out!')
    return redirect(url_for('base2'))

@users_blueprint.route('/login',methods=['GET','POST'])
def login():
    
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None:
            if user.check_password(form.password.data) and user is not None:

                login_user(user)
                # get GEO IP
                geoip_data = simple_geoip.get_geoip_data()
                try:
                    ip = geoip_data['ip']
                except:
                    ip = '<none>'

                if ip != '<none>':
                    geoipcount = UserGeoIP.query.filter_by(user_id=user.id,ip=ip).count()

                    if geoipcount <= 0:
                        newGeoIp = UserGeoIP(ip)
                        newGeoIp.country = geoip_data['location']['country']
                        newGeoIp.region = geoip_data['location']['region']
                        newGeoIp.city = geoip_data['location']['city']
                        newGeoIp.lat = geoip_data['location']['lat']
                        newGeoIp.lng = geoip_data['location']['lng']
                        newGeoIp.postalCode = geoip_data['location']['postalCode']
                        newGeoIp.timezone = geoip_data['location']['timezone']
                        newGeoIp.user_id = user.id
                        
                        db.session.add(newGeoIp)
                        db.session.commit()

                flash('Logged in Successfully!')

                next = request.args.get('next')

                if next == None or not next[0] == '/':
                    next = url_for('base2')
                
                return redirect(next)

    return render_template('login.html', form=form,ra=request.user_agent)

@users_blueprint.route('/register',methods=['GET','POST'])
def register():
    # form = RegistrationForm()

    # if form.validate_on_submit():
    #     user = User(email=form.email.data,
    #                 username=form.username.data,
    #                 password=form.password.data)

    #     db.session.add(user)
    #     db.session.commit()
    #     flash('Thanks for registration!')
        
    #     token = generate_confirmation_token(user.email)
    #     flash("Email verification was sent to your email address")
    #     confirm_url = url_for('users.confirm', token=token, _external=True)
    #     html = render_template('mail_send.html', confirm_url=confirm_url)
    #     send_email_confirmation('jpprjn@yahoo.com',html)
    #     login_user(user)

    
    #     return redirect(url_for('base2'))

    # return render_template('register.html',form=form)
    return redirect(url_for('base2'))

@users_blueprint.route('/confirm/<token>')
def confirm(token):
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired')

    user = User.query.filter_by(email=email).first_or_404()
    if user.confirmed:
        flash('Account already confirmed. Please login')
    else:
        user.confirmed = True 
        user.confirmed_on = datetime.datetime.now()
        db.session.add(user)
        db.session.commit()
        flash('You have confirmed your account. Thanks!')

    return redirect(url_for('base2'))


