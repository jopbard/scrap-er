from flask import Flask, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from tokenizer import generate_confirmation_token,confirm_token
from flask_migrate import Migrate, MigrateCommand
from bs4 import BeautifulSoup
from flask_login import LoginManager,UserMixin
from flask_mail import Mail, Message
from werkzeug.security import generate_password_hash,check_password_hash
### new
from flask_simple_geoip import SimpleGeoIP
#from flask_script import Manager
import datetime 

login_manager = LoginManager()
app = Flask(__name__)

## App Configuration ### 
app.config['SECRET_KEY'] = '<ENTER-YOUR-SECRET-KEY-HERE>'
app.config['SECURITY_PASSWORD_SALT'] = '<ENTER-YOUR-SECURITY-PASSWORD-SALT-HERE>'
app.config['SQLALCHEMY_DATABASE_URI'] = '<ENTER-YOUR-DATABASE-CONNECTION-HERE>'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False 
app.config["GEOIPIFY_API_KEY"] = "<ENTER-YOUR-GEOIPIFY-HERE>"

app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER='smtp.gmail.com',
	MAIL_PORT=465,
	MAIL_USE_SSL=True,
	MAIL_USERNAME = '<ENTER-YOUR-EMAIL-HERE>',
	MAIL_PASSWORD = '<ENTER-YOUR-APP-PASSWORD-HERE>'
	)
mail = Mail(app)

db = SQLAlchemy(app)
Migrate(app, db)
# migrate = Migrate(app, db)
# manager = Manager(app)
# manager.add_command('db', MigrateCommand)
## Initialize login_manager
login_manager.init_app(app)
login_manager.login_view = 'login'
simple_geoip = SimpleGeoIP(app)
### MODELS ### 
broker_business = db.Table('broker_business',
                db.Column('broker_id',db.Integer,db.ForeignKey('brokers.id')),
                db.Column('business_id',db.Integer,db.ForeignKey('businesses.id'))
            )
office_business = db.Table('office_business',
                db.Column('office_id', db.Integer,db.ForeignKey('offices.id')),
                db.Column('business_id', db.Integer,db.ForeignKey('businesses.id'))
            )
class Business(db.Model):
    __tablename__='businesses'
    id = db.Column(db.Integer, primary_key=True)
    listing_id = db.Column(db.String(11), nullable=False)
    title = db.Column(db.String(255))
    price_info = db.Column(db.String(255))
    link = db.Column(db.String(255))
    broker = db.Column(db.String(255))
    price_detail = db.Column(db.String(200))
    profit = db.Column(db.String(200))
    sales = db.Column(db.String(200))
    location = db.Column(db.String(200))
    industry_detail = db.Column(db.String(200))
    type_detail = db.Column(db.String(200))
    description = db.Column(db.Text)
    #new columns 4/20/2020
    updated_on = db.Column(db.DateTime, nullable=True)
    created_on = db.Column(db.DateTime, nullable=True)
    removed_on = db.Column(db.DateTime, nullable=True)
    brokers = db.relationship('Broker',secondary=broker_business,backref=db.backref('businesses',lazy='dynamic'))
    offices = db.relationship('Office',secondary=office_business,backref=db.backref('businesses',lazy='dynamic'))
    #office_id = db.Column(db.Integer, db.ForeignKey('offices.id'))
    industry_id = db.Column(db.Integer, db.ForeignKey('industries.id'))
    business_type_id = db.Column(db.Integer, db.ForeignKey('business_types.id'))
    version_logs = db.relationship('BusinessVersionLog', backref='business', lazy=True)
    # new columns 4/28/2020 
    removed = db.Column(db.Boolean, default=False)
    sold = db.Column(db.Boolean, default=False)
    country_id = db.Column(db.Integer, db.ForeignKey('countries.id'))

    def __init__(self, listing_id, title):
        self.listing_id = listing_id
        self.title = title
        self.created_on = self.now()

    def __repr__(self):
        return f"#: {self.listing_id} title: {self.title}"

    def now(self):
        return datetime.datetime.now()

class BusinessVersionLog(db.Model):
    __tablename__='business_version_logs'
    id = db.Column(db.Integer, primary_key=True)
    listing_id = db.Column(db.String(11), nullable=False)
    title = db.Column(db.String(255))
    price_info = db.Column(db.String(255))
    link = db.Column(db.String(255))
    broker = db.Column(db.String(255))
    office = db.Column(db.String(255))
    price_detail = db.Column(db.String(200))
    profit = db.Column(db.String(200))
    sales = db.Column(db.String(200))
    location = db.Column(db.String(200))
    industry = db.Column(db.String(200))
    type_detail = db.Column(db.String(200))
    description = db.Column(db.Text)
    created_on = db.Column(db.DateTime, nullable=True)
    business_id = db.Column(db.Integer,db.ForeignKey('businesses.id'))

class Industry(db.Model):
    __tablename__='industries'
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(100),unique=True,index=True)
    businesses = db.relationship('Business', backref='industry', lazy=True)
    def __init__(self, title):
        self.title = title 

class BusinessType(db.Model):
    __tablename__='business_types'
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(100),unique=True,index=True)
    businesses = db.relationship('Business', backref='business_type', lazy=True)

    def __init__(self, title):
        self.title = title 

# new table 4/28/2020
class Countries(db.Model):
    __tablename__='countries'
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(60))
    code = db.Column(db.String(3))
    businesses = db.relationship('Business', backref='country', lazy=True)
    offices = db.relationship('Office', backref='country', lazy=True)

    def __init__(self, name, code):
        self.name = name
        self.code = code

class Broker(db.Model):
    __tablename__ ='brokers'
    id = db.Column(db.Integer,primary_key=True)
    fullname = db.Column(db.String(128))
    phone = db.Column(db.String(64))
    mobile = db.Column(db.String(64))
    email = db.Column(db.String(64))
    linkedin = db.Column(db.String(64))
    office_id = db.Column(db.Integer, db.ForeignKey('offices.id'))

    def __init__(self, fullname, phone, mobile, email, linkedin):
        self.fullname = fullname
        self.phone = phone
        self.mobile = mobile 
        self.email = email

class Office(db.Model):
    __tablename__='offices'
    id = db.Column(db.Integer,primary_key=True)
    office_id = db.Column(db.String(64))
    title = db.Column(db.String(64))
    country_code = db.Column(db.String(10))
    #businesses = db.relationship('Business', backref='office', lazy=True)
    brokers = db.relationship('Broker', backref='office', lazy=True)
    # new columns 4//28/2020
    state = db.Column(db.String(60))
    country_id = db.Column(db.Integer, db.ForeignKey('countries.id'))

    def __init__(self, office_id, title, country_code):
        self.office_id = office_id
        self.title = title
        self.country_code = country_code
 
@login_manager.user_loader 
def load_user(user_id):
    return User.query.get(user_id)

class User(db.Model,UserMixin):
    
    __tablename__='users'
    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.String(64),unique=True,index=True)
    username = db.Column(db.String(64),unique=True,index=True)
    password_hash = db.Column(db.String(128))
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    suspended = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime)
    geoips = db.relationship('UserGeoIP', backref='user', lazy=True)

    def __init__(self,email,username,password):
        self.email = email 
        self.username = username 
        self.password_hash = generate_password_hash(password)
        self.registered_on = self.now()
    
    def check_password(self,password):
        return check_password_hash(self.password_hash,password)
    
    def now(self):
        return datetime.datetime.now()

class UserGeoIP(db.Model):
    __tablename__='usergeoips'
    id = db.Column(db.Integer,primary_key=True)
    ip = db.Column(db.String(60))
    country = db.Column(db.String(60))
    region = db.Column(db.String(60))
    lat = db.Column(db.String(60))
    lng = db.Column(db.String(60))
    city = db.Column(db.String(60))
    postalCode = db.Column(db.String(20))
    timezone = db.Column(db.String(20))
    created_on = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    def __init__(self, ip):
        self.ip = ip 
        self.created_on = self.now()

    def now(self):
        return datetime.datetime.now()

# class SavedFilter(db.Model):
#     pass 


### QUERY HELPERS ### 
def get_industries():
    l = []
    query = db.session.query(Business.industry_detail.distinct().label("industry_detail"))
    l = [row.industry_detail for row in query.all()]
    return l

def get_biz_types():
    l = []
    query = db.session.query(Business.type_detail.distinct().label("type_detail"))
    l = [row.type_detail for row in query.all()]
    return l

def get_industries_obj():
    all_data = Industry.query.all()
    return all_data

def get_biz_types_obj():
    all_data = BusinessType.query.all()
    return all_data
