from urls import (
    main_url,
    main_url_us,
    main_url_nz,
    main_url_au,
    pre_url,
    pre_url_us,
    pre_url_nz,
    pre_url_au,
    post_url,
    broker_url,
    broker_url_us,
    broker_url_nz,
    broker_url_au,
    contactus_url,
    broker_listing_url,
    broker_listing_url_us,
    broker_listing_url_nz,
    broker_listing_url_au,
)
import requests 
from bs4 import BeautifulSoup
def test_scrape():
    pass
    # base_url = pre_url_us + '1' + post_url
    # r = requests.get(base_url)
    # soup = BeautifulSoup(r.text, "html.parser")
    # item_count = soup.find('div', {'class':'footer-showing-result'})
    # value = get_numeric(item_count.find('span').text)
    # last_page = int(value/24)
    # if value%24 != 0:
    #     last_page = last_page + 1
    # return str(last_page)

# @param <string> country_code (e.g: US) 
# @return <list> (array/list of dict)
def scrape_card_listing(country_code):
    
    preUrl = ''
    mainUrl = ''
    if country_code == 'PH':
        preUrl = pre_url
        mainUrl = main_url
    elif country_code == 'US':
        preUrl = pre_url_us
        mainUrl = main_url_us
    elif country_code == 'NZ':
        preUrl = pre_url_nz
        mainUrl = main_url_nzß
    elif country_code == 'AU':
        preUrl = pre_url_au
        mainUrl = main_url_au
    
    # get last page number (pagination)
    base_url = preUrl + '1' + post_url
    r = requests.get(base_url)
    soup = BeautifulSoup(r.text, "html.parser")
    item_count = soup.find('div', {'class':'footer-showing-result'})
    value = get_numeric(item_count.find('span').text)
    last_page = int(value/24)
    if value%24 != 0:
        last_page = last_page + 1

    l = []
    business_count = 0
    running = True
    for page in range(0, last_page):
        page = page + 1 
        base_url = preUrl + str(page) + post_url
        # Request URL and Beautiful Parser
        r = requests.get(base_url)
        soup = BeautifulSoup(r.text, "html.parser")

        all_biz = soup.find_all('div', {"id":"bcid_11955668"})
        business_count = business_count + len(all_biz)
        try:
            if len(all_biz) <= 0:
                print(base_url)
                print(str(len(all_biz)))
                break
            for biz in all_biz:
                d = {}
                # scrape listing detail from card view
                
                pr = biz.find("div", {"class":"position-relative"})
                if pr is not None:
                    d['sold'] = True
                else:
                    d['sold'] = False
                    
                biz_info = biz.find("div", {"class":"p-3 vertical-listing"})
                biz_footer = biz.find("div", {"class":"card-footer"})
                biz_footer_div = biz_footer.find('div')
                

                try:
                    d['biz_name'] = biz_info.find('a').contents[0]
                except:
                    d['biz_name'] = '<none>'

                price_info = biz_info.find("p", {"class":"price"})
                try:
                    d['price_info'] = price_info.find("a").contents[0]
                except (IndexError, AttributeError) as e:
                    d['price_info'] = 'Refer to Broker'
                sub_link = biz_footer_div.a['href']
                biz_link = mainUrl + sub_link
                try:
                    d['biz_link'] = biz_link
                except (IndexError, AttributeError) as e:
                    d['biz_link'] = '<none>'
                try:    
                    d['listing_id'] = sub_link.split('/')[2]
                except (IndexError, AttributeError) as e:
                    d['listing_id'] = '<none>'
                try:
                    d['details'] = 'scrape_listing_details(biz_link)'
                except (IndexError, AttributeError) as e:
                    d['details'] = "<none>"
                ### PH
                try: 
                    sr = biz.find("span", text="Sales Revenue:")
                    d['sales_detail'] = sr.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['sales_detail'] = "<none>"
                try:
                    pd = biz.find("span", text="Profit*:")
                    d['profit_detail'] = pd.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['profit_detail'] = "<none>"

                try:
                    pr = biz.find("span", text="Price:")
                    d['price_detail'] = pr.find_parent('p').find('a').contents[0]
                except (IndexError, AttributeError) as e:
                    d['price_detail'] = "<none>"
                d['office_detail'] = ''
                d['description'] = ''
                try:
                    loc = biz.find("span", text="Location: ")
                    location = loc.find_parent('p').find('a').contents[0]
                    d['location_detail'] = word_trimmer(location)
                except (IndexError, AttributeError) as e:
                    d['location_detail'] = "<none>"
                d['broker_name'] = ''
                try:
                    ind = biz.find("span", text="Industry: ")
                    industry = ind.find_parent('p').find('a').contents[0]
                    d['industry_detail'] = word_trimmer(industry)
                except (IndexError, AttributeError) as e:
                    d['industry_detail'] = "<none>"
                try:
                    typ = biz.find("span", text="Type: ")
                    typeD = typ.find_parent('p').find('a').contents[0]
                    d['type_detail'] = word_trimmer(typeD)
                except (IndexError, AttributeError) as e:
                    d['type_detail'] = "<none>"
                ### PH
                l.append(d)
        except (IndexError, AttributeError) as e:
            print(str(len(all_biz)))
            print('failed')
            break

    return l

# Get brokers' businesses
# @param <string> name (e.g. Mia Khalifa)
# @param <string> country_code (e.g. US) 
# @return <list> (list/array of <string> business Ids)
def get_broker_biz(name, country_code):
    preUrl = ''
    if country_code == 'PH':
        preUrl = broker_listing_url
    elif country_code == 'US':
        preUrl = broker_listing_url_us
    elif country_code == 'NZ':
        preUrl = broker_listing_url_nz
    elif country_code == 'AU':
        preUrl = broker_listing_url_au

    l = []
    name_l = name.split(" ")
    slug_name = "-".join(name_l)
    base_url = preUrl + slug_name
    
    for page in range(0, 9):
        page = page + 1
        url = base_url + '?page='+ str(page)
        print(slug_name + " - page: " + str(page))
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")

        try:
            listings = soup.find_all('div', {'id':'bcid_11955668'})
            if len(listings) <= 0:
                print("breaking")
                break
            for listing in listings:
                print('listing true')
                listing_footer = listing.find("div", {"class":"card-footer"})
                listing_footer_div = listing_footer.find('div')
                sub_link = listing_footer_div.a['href']
                l.append(sub_link.split('/')[2])
        except (IndexError, AttributeError) as e:
            print('listing false')
            running = False

    print("returning")
    return l 

# Remove unnecessary word(s)
# @param <string> str (untrimmed word(s))
# @return <string>
def word_trimmer(str):
    if (str.find('business') != -1):
        x = str.split(" business", 2)
        return x[0] 
    else:
        if (str.find('freehold') != -1):
            x = str.split(" freehold", 2)
            return x[0]
        else:
            return str

# Get offices by country
# @param <string> country_code (e.g. US)
# @return <list> [{'value':<string>,'text':<string>},...] (e.g. value: 'AL0000', text: 'Alabang Office')
def get_office(country_code):
    contactUsUrl = ''
    if country_code == 'PH':
        contactUsUrl = main_url + contactus_url
    elif country_code == 'US':
        contactUsUrl = main_url_us + contactus_url
    elif country_code == 'NZ':
        contactUsUrl = main_url_nz + contactus_url
    elif country_code == 'AU':
        contactUsUrl = main_url_au + contactus_url

    l = []
    
    s = requests.Session()
    r = s.get(contactUsUrl)
    soup = BeautifulSoup(r.text, 'html.parser')
    offices = soup.find('select', {'id': 'OfficeId'})
    for option in offices.find_all('option'):
        d = {}
        if option['value'] == '':
            continue
        d['value'] = option['value']
        d['text'] = option.text
        l.append(d)

    return l
    
# @param <string> officeId
# @param <string> country_code (e.g. US)
# @return <list> (list of dict)
def get_brokers(officeId, country_code):
    contactUsUrl = ''
    brokerUrl = ''
    if country_code == 'PH':
        contactUsUrl = main_url + contactus_url
        brokerUrl = broker_url
    elif country_code == 'US':
        contactUsUrl = main_url_us + contactus_url
        brokerUrl = broker_url_us
    elif country_code == 'NZ':
        contactUsUrl = main_url_nz + contactus_url
        brokerUrl = broker_url_nz
    elif country_code == 'AU':
        contactUsUrl = main_url_au + contactus_url
        brokerUrl = broker_url_au

    l = []
    s = requests.Session()
    r = s.get(contactUsUrl)
    token = BeautifulSoup(r.text, 'html.parser').find('input',{'name':'__RequestVerificationToken'})['value']
    payload = {'OfficeId':officeId,
                'BrokerName':'',
                '__RequestVerificationToken': token}
    p = s.post(brokerUrl, data=payload) 
    soup = BeautifulSoup(p.text, 'html.parser')
    print(soup.title)
    search_results = soup.find("div", {"class":"webappsearchresults"})
    all_brokers = search_results.find_all('div', {"class":"team-member"})
    for broker in all_brokers:
        d = {}
        details = broker.find("div", {"class":"teamdetails"})
        d['name'] = details.find("a", {"class":"member-name"}).contents[0]
        try:
            d['phone'] = details.find("div", {"class":"member-phone"}).find("span", {"class":"infoValue"}).contents[0]
        except (IndexError, AttributeError) as e:
            d['phone'] = ''
        try:
            d['mobile'] = details.find("div", {"class":"member-mobile"}).find("span", {"class":"infoValue"}).contents[0]
        except (IndexError, AttributeError) as e:
            d['mobile'] = ''
        try:
            d['email'] = details.find("div", {"class":"member-email"}).find("a", {"class":"infoValue"}).contents[0]
        except (IndexError, AttributeError) as e:
            d['email'] = ''
        l.append(d)
    return l

def get_numeric(str):
    x = str.split(' items.', 2)
    return int(x[0])