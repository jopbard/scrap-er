from flask import Blueprint,render_template,redirect,url_for,flash,abort,request,make_response
from tokenizer import generate_confirmation_token,confirm_token
from flask_login import current_user
from app import (
    Business,
    get_industries_obj,
    get_biz_types_obj,
)
from forex_python.converter import CurrencyRates
import datetime 
nz_blueprint = Blueprint('nz',__name__)

@nz_blueprint.route('/test')
def test():

    return 'success'

@nz_blueprint.route('/listing') 
def listing():
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))

    all_biz = Business.query.filter_by(country_id=4).all()
    i = get_industries_obj()
    t = get_biz_types_obj()
    cookieSet = True
    if 'nzd' not in request.cookies:
        return redirect(url_for('index'))
    else:
        curval = round(float(request.cookies.get("nzd")), 2)

    response = make_response(render_template('index_nz.html', all_biz=all_biz, i=i,t=t,ra=request.user_agent,curval=curval))
    if cookieSet == False:
        response.set_cookie('nzd', str(x), expires=expire_date)
    return response 
