from flask import Blueprint,render_template,redirect,url_for,flash,abort,request
from tokenizer import generate_confirmation_token,confirm_token
from flask_login import current_user
from app import (
    Business,
    get_industries_obj,
    get_biz_types_obj,
)

ph_blueprint = Blueprint('ph',__name__)

@ph_blueprint.route('/test')
def test():

    return 'success'

@ph_blueprint.route('/listing') 
def listing():
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))

    all_biz = Business.query.filter_by(country_id=1).all()
    i = get_industries_obj()
    t = get_biz_types_obj()
    return render_template('index2.html', all_biz=all_biz, i=i,t=t,ra=request.user_agent)
