from flask import Blueprint,render_template,redirect,url_for,flash,abort,request, make_response
from tokenizer import generate_confirmation_token,confirm_token
from flask_login import current_user
from app import (
    Business,
    get_industries_obj,
    get_biz_types_obj,
)
from forex_python.converter import CurrencyRates
import datetime 
us_blueprint = Blueprint('us',__name__)

@us_blueprint.route('/test')
def test():

    return 'success'



@us_blueprint.route('/listing') 
def listing():
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))

    all_biz = Business.query.filter_by(country_id=2).all()
    i = get_industries_obj()
    t = get_biz_types_obj()
    cookieSet = True
    if 'usd' not in request.cookies:
        return redirect(url_for('index'))
    else:
        curval = round(float(request.cookies.get("usd")), 2)

    response = make_response(render_template('index_us.html', all_biz=all_biz, i=i,t=t,ra=request.user_agent,curval=curval))
    if cookieSet == False:
        response.set_cookie('usd', str(x), expires=expire_date)
    return response 
    
