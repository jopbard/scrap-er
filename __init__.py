from flask import Flask, render_template, url_for, redirect, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate 
from bs4 import BeautifulSoup
from flask_login import LoginManager,UserMixin,current_user
from flask_mail import Mail, Message

from werkzeug.security import generate_password_hash,check_password_hash
import datetime

from app import (
    app,
    login_manager,
    db,
    mail,
    simple_geoip,
    Business,
    User,
    BusinessType,
    Industry,
    Broker,
    Office,
    get_industries,
    get_industries_obj,
    get_biz_types,
    get_biz_types_obj,
)
import requests 
from forex_python.converter import CurrencyRates

import re
from re import sub
from decimal import Decimal


from scraper import scrape_card_listing, test_scrape, get_broker_biz, get_office, get_brokers
import json
### BLUEPRINTS ### 
from view_user import users_blueprint
from countries.ph.views import ph_blueprint 
from countries.nz.views import nz_blueprint
from countries.us.views import us_blueprint
from countries.au.views import au_blueprint
from stats.views import stat_blueprint

app.register_blueprint(users_blueprint,url_prefix='/users')
app.register_blueprint(ph_blueprint,url_prefix='/ph')
app.register_blueprint(nz_blueprint,url_prefix='/nz')
app.register_blueprint(us_blueprint,url_prefix='/us')
app.register_blueprint(au_blueprint,url_prefix='/au')
app.register_blueprint(stat_blueprint,url_prefix='/stat')
   
def extract_nums_from_curval(s):
    d = {}
    result = re.findall(r'(?:[\£\$\€\₱]{1}[,\d]+.?\d*)',s)
    if len(result) <= 0:
        money = s 
    else:
        money = result[0]
    value = Decimal(sub(r'[^\d.]', '', money))
    print(value)
    d['value'] = str(value)
    list_string = s.split(' ')
    list_string.pop(0)
    remark = ' '.join(list_string)
    d['remark'] = remark
    return d

def create_update_db(l, country_code):
    countryID = 1
    if country_code == 'PH':
        countryID = 1
    elif country_code == 'US':
        countryID = 2
    elif country_code == 'AU':
        countryID = 3
    else:
        countryID = 4
    queue_list = 0

    for biz in l:
        queue_list = queue_list + 1
        exists = db.session.query(Business.id).filter_by(listing_id=biz['listing_id']).scalar() is not None
        if exists:
            continue 
        else:
            new_biz = Business(biz['listing_id'], biz['biz_name'])
            if biz['price_info'] == '<none>' or biz['price_info'] == 'Refer to Broker' or biz['price_info'] == '':
                new_biz.price_info = biz['price_info']
                new_biz.price_detail = biz['price_info']
            else:
                d = extract_nums_from_curval(biz['price_info'])
                new_biz.price_info = d['value']
                new_biz.price_detail = d['remark']
            new_biz.link = biz['biz_link']
            new_biz.broker = biz['broker_name']
            if biz['profit_detail'] == '<none>' or biz['profit_detail'] == 'Refer to Broker' or biz['profit_detail'] == '':
                new_biz.profit = biz['profit_detail']
            else:
                d = extract_nums_from_curval(biz['profit_detail'])
                new_biz.profit = d['value']
            if biz['sales_detail'] == '<none>' or biz['sales_detail'] == 'Refer to Broker' or biz['sales_detail'] == '':
                new_biz.sales = biz['sales_detail']
            else:
                d = extract_nums_from_curval(biz['sales_detail'])
                new_biz.sales = d['value']
            new_biz.location = biz['location_detail']
            new_biz.industry_detail = biz['industry_detail']
            new_biz.type_detail = biz['type_detail']
            new_biz.description = biz['description']
            new_biz.country_id = countryID
            new_biz.sold = biz['sold']
            db.session.add(new_biz)
            if queue_list == 50:
                db.session.commit()
                queue_list = 0

    db.session.commit()
@app.template_filter()
def numberFormat(value):
    return format(int(float(value)), ',d')
    
@app.template_filter()
def USDtoPHP(value):
    r = round(float(request.cookies.get("usd")),2) 
    x = float(value) * r
    return format(int(x), ',d') 

@app.template_filter()
def AUDtoPHP(value):
    r = round(float(request.cookies.get("aud")),2) 
    x = float(value) * r
    return format(int(x), ',d') 

@app.template_filter()
def NZDtoPHP(value):
    r = round(float(request.cookies.get("nzd")),2) 
    x = float(value) * r
    return format(int(x), ',d')  

@app.route("/")
def index():
    if 'country_code' in request.cookies:
        country = request.cookies.get("country_code")
        if country == 'PH':
            response = make_response(redirect(url_for('stat.test')))
        elif country == 'AU':
            response = make_response(redirect(url_for('stat.test')))
        elif country == 'NZ':
            response = make_response(redirect(url_for('stat.test')))
        elif country == 'US':
            response = make_response(redirect(url_for('stat.test')))
        if 'usd' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('USD', 'PHP'))
            expire_date = datetime.datetime.now()
            expire_date = expire_date + datetime.timedelta(minutes=90)
            response.set_cookie('usd', x, expires=expire_date)
        if 'aud' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('AUD', 'PHP'))
            expire_date = datetime.datetime.now()
            expire_date = expire_date + datetime.timedelta(minutes=90)
            response.set_cookie('aud', x, expires=expire_date)
        if 'nzd' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('NZD', 'PHP'))
            expire_date = datetime.datetime.now()
            expire_date = expire_date + datetime.timedelta(minutes=90)
            response.set_cookie('nzd', x, expires=expire_date)
        return response 
    else:
        response = make_response(redirect(url_for('stat.test')))
        expire_date = datetime.datetime.now()
        expire_date = expire_date + datetime.timedelta(minutes=90)
        if 'usd' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('USD', 'PHP'))
            
            response.set_cookie('usd', x, expires=expire_date)
        if 'aud' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('AUD', 'PHP'))

            response.set_cookie('aud', x, expires=expire_date)
        if 'nzd' not in request.cookies:
            c = CurrencyRates()
            x = str(c.get_rate('NZD', 'PHP'))

            response.set_cookie('nzd', x, expires=expire_date)
        response.set_cookie('country_code', 'PH')
        return response

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/base2')
def base2():
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))

    return redirect(url_for('stat.test'))

@app.route('/biz/<string:listing_id>')
def biz(listing_id):
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))
        
    biz = Business.query.filter_by(listing_id=listing_id).first()
    i = get_industries_obj()
    t = get_biz_types_obj()
    return render_template('biz.html', biz=biz, i=i,t=t,ra=request.user_agent)

@app.route('/industry/<int:id>')
def industry(id):
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))
    i = get_industries_obj()
    t = get_biz_types_obj()
    industry = Industry.query.filter_by(id=id).first()
    all_biz = Business.query.filter_by(industry=industry.title).all()
    return render_template('index2.html', all_biz=all_biz, i=i,t=t,ra=request.user_agent, title=industry.title)

@app.route('/biz_type/<int:id>')
def biz_type(id):
    if current_user.is_authenticated is not True:
        return redirect(url_for('users.login'))
    i = get_industries_obj()
    t = get_biz_types_obj()
    biz_type = BusinessType.query.filter_by(id=id).first()
    all_biz = Business.query.filter_by(type_detail=biz_type.title).all()
    return render_template('index2.html', all_biz=all_biz, i=i,t=t,ra=request.user_agent, title=biz_type.title)


@app.route('/updater_1')
def updater_1():
    # Update/Add businesses to database 
    # biz_arr = scrape_card_view()
    biz_arr = scrape_card_listing('NZ')
    create_update_db(biz_arr, 'NZ')
    office_list = get_office('NZ')
    # update office table
    create_update_office_db(office_list, 'NZ')

    return 'UPDATER_1: DONE'

@app.route('/updater_2')
def updater_2():
    # Update/Add industries and business types
    biz_types = get_biz_types()
    industries = get_industries()

    for biz in biz_types:
        b = BusinessType.query.filter_by(title=biz).count()
        if b <= 0:
            new_biz_type = BusinessType(biz)
            db.session.add(new_biz_type)
            db.session.commit()
    for i in industries:
        ii = Industry.query.filter_by(title=i).count()
        if ii <= 0:
            new_industry = Industry(i)
            db.session.add(new_industry)
            db.session.commit()
    # update businesses 
    all_biz = Business.query.all()
    for biz in all_biz:
        industry = Industry.query.filter_by(title=biz.industry_detail).first()
        biz.industry_id = industry.id 
        business_type = BusinessType.query.filter_by(title=biz.type_detail).first()
        biz.business_type_id = business_type.id
        db.session.add(biz)
    db.session.commit()

    return 'done'
@app.route('/testtt')
def testtt():
    biz = Business.query.filter_by(listing_id='OR00402').first()
    b = Broker.query.filter_by(fullname='Roberto Narciso').first()
    exists = b.businesses.filter(Business.id == biz.id).scalar() is not None
    if exists:
        return 'exists'
    else:
        return "doesn't exists"
    
@app.route('/add-brokers')
def add_brokers():
    all_offices = Office.query.all()
    for office in all_offices:
        if office.country_id != 4:
            continue
        
        brokers = get_brokers(office.office_id, 'NZ')
        for broker in brokers:
            new_broker = Broker(broker['name'],broker['phone'],broker['mobile'],broker['email'],'-----')
            new_broker.office_id = office.id
            db.session.add(new_broker)
        db.session.commit()
    return "done"

@app.route('/updater_3')
def updater_3():
    # business-broker
    l = []
    #brokers = Broker.query.filter_by().all()
    brokers = db.session.query(Broker).join(Office).filter(Office.country_id==4).all()
    for broker in brokers:
        print("broker: " + broker.fullname + "office: " + str(broker.office_id)) 
        listing_ids = get_broker_biz(broker.fullname, "NZ")
        for listing_id in listing_ids:
            biz_exists = db.session.query(Business.id).filter_by(listing_id=listing_id).scalar() is not None
            if biz_exists:
                # check if relationship does exists with broker and business
                biz = Business.query.filter_by(listing_id=listing_id).first()
                relationsiop_exists = broker.businesses.filter(Business.id == biz.id).scalar() is not None
                if relationsiop_exists:               
                    print('relationship exists')
                    continue
                else:
                    broker.businesses.append(biz)
            else:
                continue
        db.session.commit()
        print("business appended")
    
    return 'done'

@app.route('/set-country/<string:country_code>')
def set_country(country_code):
    if country_code == 'PH':
        response = make_response(redirect(url_for('ph.listing')))
    elif country_code == 'AU':
        response = make_response(redirect(url_for('au.listing')))
    elif country_code == 'NZ':
        response = make_response(redirect(url_for('nz.listing')))
    elif country_code == 'US':
        response = make_response(redirect(url_for('us.listing')))
    else:
        response = make_response(redirect(url_for('ph.listing')))
    response.set_cookie('country_code', country_code)
    return response 
if __name__ == "__main__":
    app.run(debug=True)

