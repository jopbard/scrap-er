from itsdangerous import URLSafeTimedSerializer
sk = 'mys3cr3tk3y'
sps = 'h4y4h4y'
def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(sk)
    return serializer.dumps(email, salt=sps)


def confirm_token(token):
    serializer = URLSafeTimedSerializer(sk)
    try:
        email = serializer.loads(
            token,
            salt=sps,
            max_age=3600
        )

    except Exception as e:
        return False
    
    return email
